// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import adapter from "./common/js/adapter"
require('swiper/dist/css/swiper.css')
Vue.config.productionTip = false;
// Vue.prototype.miguapi = 'http://test.api.wukongtv.com/';
Vue.prototype.miguapi = 'https://api.wukongtv.com/';
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
