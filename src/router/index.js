import Vue from 'vue'
import HomePage from '../components/homepage/homepage.vue'
import BuyOrShow from '../components/buyorshow/buyorshow.vue'
import Gamelists from '../components/gamelists/gamelists.vue'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueClipboard from 'vue-clipboard2'
import VueScroller from 'vue-scroller'

Vue.use(Router)
Vue.use(VueResource)
Vue.use(VueAwesomeSwiper)
Vue.use(VueClipboard)
Vue.use(VueScroller)

let router = new Router({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage
    },
    {
      path: '/buyorshow/:planId',
      name: 'BuyOrShow',
      component: BuyOrShow
    },
    {
      path: '/gamelists',
      name: 'GameList',
      component: Gamelists
    }
  ]
})
export default router
