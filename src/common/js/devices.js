/**
 * Created by gladyu on 17/9/13.
 */
var browser = {
  versions: function () {
    var u = navigator.userAgent, app = navigator.appVersion;
    return {//移动终端浏览器版本信息
      iOS: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
      android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1,
      isWeixin: u.toLowerCase().match(/MicroMessenger/i) == "micromessenger",
      isQQ: u.toLowerCase().indexOf('mqqbrowser') > -1 && u.toLowerCase().indexOf('qq/') > -1,
      isWeibo: u.toLowerCase().indexOf('weibo') > -1,
    };
  }()
}
exports.getUrlBaseArg = function(){
  var c = window.WKAPI.getHostChannel() || '';
  var v = window.WKAPI.getHostVersion() || '';
  var dev = browser.versions.iOS ? "ios" : "android";
  var vkey = browser.versions.iOS ? "iv":"v";
  return "c=" + c + "&" + vkey + "=" + v + "&dev=" + dev;
}
exports.getCookie = function(c_name) {
  if (document.cookie.length > 0) {
    c_start = document.cookie.indexOf(c_name + "=")
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1
      c_end = document.cookie.indexOf(";", c_start)
      if (c_end == -1) c_end = document.cookie.length
      return unescape(document.cookie.substring(c_start, c_end))
    }
  }
  return ""
}
